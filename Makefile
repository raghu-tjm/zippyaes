CFLAGS := -ggdb -Wall -O2
OMPFLAGS := -fopenmp
AESNI := -maes

all: aes_ni.o aes.o

all128: seq128 slurp128 ni128

all192: seq192 slurp192 ni192

all256: seq256 slurp256 ni256

aes_ni.o: aes_ni.c aes_ni.h
	gcc $(CFLAGS) $(AESNI) $(OMPFLAGS) -c $< -o $@

aes.o: aes_core.c aes_locl.h aes.h
	gcc $(CFLAGS) -c $< -o $@

seq: seq128 seq192 seq256

seq128: test_seq128.c aes.o
	gcc $(CFLAGS) $^ -o $@

seq192: test_seq192.c aes.o
	gcc $(CFLAGS) $^ -o $@

seq256: test_seq256.c aes.o
	gcc $(CFLAGS) $^ -o $@

slurp: slurp128 slurp192 slurp256

slurp128: test_slurp128.c aes.o
	gcc $(CFLAGS) $^ -o $@

slurp192: test_slurp192.c aes.o
	gcc $(CFLAGS) $^ -o $@

slurp256: test_slurp256.c aes.o
	gcc $(CFLAGS) $^ -o $@

ni: ni128 ni192 ni256

ni128: test_ni128.c aes_ni.o
	gcc $(CFLAGS) $(AESNI) $(OMPFLAGS) $^ -o $@

ni192: test_ni192.c aes_ni.o
	gcc $(CFLAGS) $(AESNI) $(OMPFLAGS) $^ -o $@

ni256: test_ni256.c aes_ni.o
	gcc $(CFLAGS) $(AESNI) $(OMPFLAGS) $^ -o $@



clean:
	rm -f *.o test buff seq omp slurp256 slurp192 slurp128 ni
	rm *.bin
	rm plaintext_decrypted.txt

