#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <time.h>
#include "aes.h"


int main()
{

    static const unsigned char key[16] = "helloworldhello!";

    unsigned char buff[16];
    unsigned char enc_in[16];
    unsigned char enc_out[16];
    unsigned char dec_out[16];
    clock_t begin, end;
    double time_elapsed;

    FILE *fpr;
    FILE *fpwe;

    fpr = fopen("plaintext.txt", "r");
    fpwe = fopen("plaintext_encrypted.bin", "wb");


    AES_KEY enc_key, dec_key;

    printf("Encrypting...\n");

    begin = clock();

    AES_set_encrypt_key(key, 128, &enc_key);


    while(fgets(buff, 16, fpr)!=NULL) {
       
        AES_encrypt(buff, enc_out, &enc_key); 
        fwrite(enc_out, sizeof(enc_out), 1, fpwe);

    }

    end = clock();
    time_elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
    
    fclose(fpr);
    fclose(fpwe);


    printf("Encryption completed in %f seconds\n", time_elapsed);
    


    FILE *fpre;
    FILE *fwd;

    fpre = fopen("plaintext_encrypted.bin", "rb");
    fwd = fopen("plaintext_decrypted.txt", "a");

    printf("Decrypting....\n");

    begin = clock();

    AES_set_decrypt_key(key,128, &dec_key);

    while(fread(buff, sizeof(buff), 1, fpre)) {
        AES_decrypt(buff, dec_out, &dec_key);  
        fprintf(fwd,"%s",dec_out);
    }
    fclose(fpre);
    fclose(fwd);

    end = clock();
    time_elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Decryption completed in %f seconds\n", time_elapsed);


    return 0;
}
