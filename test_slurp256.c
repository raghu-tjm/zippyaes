/** @file test_slurp256.c
 *  @brief A fast buffered interface to perform AES-256 encryption.
 *  
 *  A faster AES-256 interface to utilize "aes_core.c", an OpenSSL based implementation of 
 *  AES cryptographic library. Provides a 256-bit key (32 bytes) to key expansion functions. 
 *  Slurps "plaintext.txt" into a read buffer and uses encrypt  function of  "aes_core.c"
 *  for encryption. Encrypted output is written block-wise to the "plaintext_encrypted.bin",
 *  a binary file. In decryption, "plaintext_encrypted.bin" is similarly slurped, decrypted 
 *  using decrypt function, and buffer written to file "plaintext_decrypted.txt". 
 * 
 *  @author Raghu Mulukutla <raghu94@cmu.edu>
 *  @date 4/28/16
 *  @bug No known bugs
 * 
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "aes.h"



int main()
{

    //================================================================================
    // Common Declarations
    //================================================================================

    static const unsigned char key[32] = "hello, AES block cipher, stream"; 

    unsigned char *rbuff; //read buffer for slurping input file
    unsigned char *wbuff; //write buffer for writing encrypted file
    unsigned char enc_in[16];
    unsigned char enc_out[16];
    unsigned char dec_out[16];
    unsigned char dec_in[16];
    clock_t begin, end;
    double time_elapsed;
    int length;
    int iterator;



    //================================================================================
    // Encryption
    //================================================================================

    FILE *fpr;
    FILE *fpwe;
    


    fpr = fopen("plaintext.txt", "r");  //input
    fpwe = fopen("plaintext_encrypted.bin", "wb"); //encrypted output


    if((fpr == NULL)||(fpwe == NULL))
        return(-1);

    fseek(fpr, 0, SEEK_END);
    length = ftell(fpr);
    fseek(fpr, 0, SEEK_SET);


    rbuff = malloc(sizeof(unsigned char)*length);
    wbuff = malloc(sizeof(unsigned char)*length);


    AES_KEY enc_key;

    printf("\nEncrypting with 256-bit key :\t");
    for (int i = 0; i < 32; ++i)
        printf("%c",key[i]);
    printf("\n");


    begin = clock();
    AES_set_encrypt_key(key, 256, &enc_key);

    printf("\nSlurping file of length %d bytes into memory\n", length );

    if(rbuff) 
        fread(rbuff, sizeof(unsigned char), length, fpr); //read input file into rbuff

    iterator = 16*(length / 16);

    for(int i=0; i < iterator; i=i+16) {
        memcpy(enc_in, (rbuff+i), 16);
        AES_encrypt(enc_in, enc_out, &enc_key);
        memcpy((wbuff+i), enc_out, sizeof(enc_out));
    }

    fwrite(wbuff, sizeof(unsigned char), length, fpwe);

    end = clock();
    time_elapsed = (double)(end - begin) / CLOCKS_PER_SEC;

    free(wbuff);
    free(rbuff);
    fclose(fpr);
    fclose(fpwe);


    printf("\nEncryption has completed in %f seconds\n", time_elapsed);


    
    //================================================================================
    // Decryption
    //================================================================================

    FILE *fpre;
    FILE *fwd;

    

    fpre = fopen("plaintext_encrypted.bin", "rb");
    fwd = fopen("plaintext_decrypted.txt", "w");

    if((fpre == NULL)||(fwd == NULL))
        return(-1);

    fseek(fpre,0, SEEK_END);
    length = ftell(fpre);
    fseek(fpre,0, SEEK_SET);

    rbuff = malloc(sizeof(unsigned char)*length);
    wbuff = malloc(sizeof(unsigned char)*length);


    AES_KEY dec_key;

    printf("\nDecrypting with 256-bit key :\t");
    for (int i = 0; i < 32; ++i)
        printf("%c",key[i]);
    printf("\n");

    begin = clock();
    AES_set_decrypt_key(key, 256, &dec_key);

    printf("\nSlurping file of length %d bytes into memory\n", length );
    
    if(rbuff) 
        fread(rbuff, sizeof(unsigned char), length, fpre); //read input file into rbuff
    
    
    iterator = 16*(length / 16);

    for (int i = 0; i < iterator; i=i+16) 
    {
        memcpy(dec_in, (rbuff+i),16);
        AES_decrypt(dec_in, dec_out, &dec_key);
        memcpy((wbuff+i), dec_out, sizeof(dec_out));
    }
    
    fwrite(wbuff, sizeof(unsigned char), length, fwd );


    end = clock();
    time_elapsed = (double)(end - begin) / CLOCKS_PER_SEC;

    free(rbuff);
    free(wbuff);
    fclose(fpre);
    fclose(fwd);
    

    printf("\nDecryption has completed in %f seconds\n\n", time_elapsed);


    return 0;
}
