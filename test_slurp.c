#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include "aes.h"


int main()
{

    static const unsigned char key[16] = "hey cryptography";

    unsigned char *rbuff; //read buffer for slurping input file
    unsigned char *wbuff; // write buffer for writing encrypted file
    unsigned char enc_in[16];
    unsigned char enc_out[16];
    unsigned char dec_out[16];
    clock_t begin, end;
    double time_elapsed;
    int i;
    int length;

    FILE *fpr;
    FILE *fpwe;
    FILE *sfp;

    fpr = fopen("plaintext.txt", "r");
    fpwe = fopen("plaintext_encrypted.bin", "wb");
    sfp = fopen("decrypted.txt", "w");

    if(fpr == NULL) {
    	
    	return(-1);
    }

    if(sfp == NULL) {
        
        return(-1);
    }


    fseek(fpr, 0, SEEK_END);

    length = ftell(fpr);

    printf("Total file size is = %d bytes\n", length );

    fseek(fpr, 0, SEEK_SET);


    rbuff = malloc(sizeof(unsigned char)*length);
    wbuff = malloc(sizeof(unsigned char)*length);



    AES_KEY enc_key, dec_key;

    printf("Encrypting...\n");
    begin = clock();
    AES_set_encrypt_key(key, 128, &enc_key);

    if(rbuff) 
        fread(rbuff, sizeof(unsigned char), length, fpr); // read input file into rbuff
    

    

    length = strlen(rbuff);
    //printf("%s\n",rbuff );


   
    for(int i=0; i < length; i=i+15) {
        memcpy(enc_in, (rbuff+i), 15);
        AES_encrypt(enc_in, enc_out, &enc_key);
        
        //memcpy((wbuff+i), enc_out, sizeof(enc_out));
        fwrite(enc_out, sizeof(unsigned char), sizeof(enc_out), fpwe);

    }

  
   // fwrite(wbuff, sizeof(unsigned char), length, fpwe);

   
    

    

    // if(wbuff)
    //     fwrite(wbuff, 1, length, fpwe);

    // for (int i = 0; i <length; ++i)
    // {
    //     printf("%c",*(wbuff+i));
    // }

    free(wbuff);
    free(rbuff);
    // return 0;

    
    //fwrite(wbuff, sizeof(wbuff), 1, fpwe);

    
    
    // return 0;

    // while(fgets(buff, 4095, fpr)) {
    //     buff[4096] = '\0';
    //     length = strlen(buff);
    //     for(i=0;i<length;i=i+15) {
    //         memcpy(enc_in, (buff+i), 15);
    //         enc_in[15]='\0';
    //         AES_encrypt(enc_in, enc_out, &enc_key); 
    //         fwrite(enc_out, sizeof(enc_out), 1, fpwe);
    //     }
    // }
 

    end = clock();
    time_elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
    
    fclose(fpr);
    fclose(fpwe);


    printf("Encryption completed in %f seconds\n", time_elapsed);
    


    FILE *fpre;
    FILE *fwd;

    unsigned char debuff[16];

    fpre = fopen("plaintext_encrypted.bin", "rb");
    fwd = fopen("plaintext_decrypted.txt", "a");

    printf("Decrypting....\n");

    begin = clock();

    AES_set_decrypt_key(key,128, &dec_key);

    while(fread(debuff, sizeof(debuff), 1, fpre)) {
        AES_decrypt(debuff, dec_out, &dec_key);  
        fprintf(fwd,"%s",dec_out);
    }
    fclose(fpre);
    fclose(fwd);

    end = clock();
    time_elapsed = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Decryption completed in %f seconds\n", time_elapsed);


    return 0;
}
